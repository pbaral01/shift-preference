import collections
import re
import sys
import time
import string

def write_text(text_list, file_path):
    with open(file_path, 'w') as text_file:
        text_file.writelines("\n".join(text_list))
    return 

    
def read_text(file_path):
    with open(file_path, 'r') as text_file:
        text = text_file.readlines()
    text = [item.replace("\n", "") for item in text]
    return text

def tokenize(string_):
    string_ = re.sub(' +',' ',string_)
    string_ = string_.translate(str.maketrans("","", '!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~'))
    return string_.lower().split(" ")


def count_ngrams(lines, min_length=2, max_length=4):

    lengths = range(min_length, max_length + 1)
    ngrams = {length: collections.Counter() for length in lengths}
    queue = collections.deque(maxlen=max_length)

    # Helper function to add n-grams at start of current queue to dict
    def add_queue():
        current = tuple(queue)
        for length in lengths:
            if len(current) >= length:
                ngrams[length][current[:length]] += 1

    # Loop through all lines and words and add n-grams to dict
    for line in lines:
        for word in tokenize(line):
            queue.append(word)
            if len(queue) >= max_length:
                add_queue()

    # Make sure we get the n-grams at the tail end of the queue
    while len(queue) > min_length:
        queue.popleft()
        add_queue()

    return ngrams

stopwords = '''a
all
an
and
are
as
at
be
but
by
for
if
in
into
is
it
my
near
of
on
or
other
s
such
t
that
the
their
then
there
these
they
this
to
was
will
with'''

def stop_words():
    return stopwords.split("\n")

