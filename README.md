# README #


### What is this repository for? ###

This repository consists of code used for creating and evaluating regular expressions and dictionary for shift preference. Shift preference deals with trying to classify a given G2 comment or a Job description into first, second, third or weekend shift. 

The business ask was to generate a dictionary of terms/corresponding regex commonly encountered in G2 comments that describe a candidate's preference for first shift, second shift, third shift or weekend shift.

There is a jupyter notebook file that was used on GCP Vertex AI notebooks for the exploration and delivery of the keywords/regex.

This repository serves to store the process that was taken for the work done on iteration 4.1 (2021) by DSIA.

### SnowFlake Queries ###
#### for G2 comment ####
select id, description 
from "DATA_LAKE"."SFDC"."TASK"
where  (createddate is not null or  createddate != 'NULL' or  createddate is not NULL)
and type = 'G2'
and lower(description) like '%shift%' 

#### for job code description ####

select id, Req_Job_Description__c
from "DATA_LAKE"."SFDC"."OPPORTUNITY" 
where opco__c in ('Aerotek','Aerotek, Inc.','TEKsystems, Inc.','Aerotek, Inc','Allegis Group, Inc','
Allegis Group, Inc.') 
and lower(Req_Job_Description__c) like '%shift%'

